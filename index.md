---
title: Tracking for Safety
description: Using RTLS to track workers for the insurance of their safety 
author: Omar Yousef and Robin Berweiler
keywords: safety, tracking, workers
url: https://omarrfaroukk.gitlab.io/slide-template-yousef-omar
marp: true
image: ![50%](assets/safety_avatar.jpg)
---

# Tracking for Safety
 
![w:800 h:500](assets/tüvimg.png)

_[img1]_

---

# About Us

![w:1100 h:500](assets/picture_of_omar_and_robin.jpg)

---

# Problem Definition

![](assets/safety_problems.png)

---

# Solution Concept

![](assets/safety_solutions.png)

---

# Sewio

![w:800 h:500](assets/workers_tracking_sewio.gif)

_[img2]_

---

# BlueIOT

![w:800 h:500](assets/workers_tracking_bluiot.png)

_[img3]_

---

# Importance of Concept

#### Social

Companies with less accidents and less faults in construction usually are better viewed by citizens. 
higher social stand --> more applications and job offers --> more sales increasing reputation

#### Economical

If workers are safer they usually work faster and more.
worker safety --> higher efficiency --> earlier deadlines

#### Environmental

Monitor construction and detect vibrations. 
high vibration in some part --> prevent vibration --> increase lifecycle --> less pollution

---

#### Video Tracking Option

![w:800 h:500](assets/tracking.gif)

_[img4]_

---

#Sources

_[img1]_: https://www.tuv.com/world/en/construction-site-safety-training.html

_[img2]_: https://www.sewio.net/people-employee-indoor-location-tracking-and-monitoring/

_[img3]_: https://www.blueiot.com

_[img4]_: https://github.com/apoorvavinod/Real_time_Object_detection_and_tracking

---

# Vielen Dank!