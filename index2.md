---
title: Tracking for Safety
description: Using RTLS to track workers for the insurance of their safety 
author: Omar Yousef and Robin Berweiler
keywords: safety, tracking, workers
url: https://omarrfaroukk.gitlab.io/slide-template-yousef-omar
marp: true
image: ![50%](assets/safety_avatar.jpg)
---

# Tracking for Safety

![w:240,4 h:180](./assets/tüvimg.png)

_[img1]_

---
# About Us

![20%](./assets/picture_of_omar_and_robin.jpg)

**Architect** (Omar) and **Mechanical Engineer** (Robin)

---

# Problem Definition

![70%](./assets/safety_problems.png)

---

# Solution Concept

![bg 70%](./assets/safety_solutions.png)

---

# Sewio

![bg right](assets/workers_tracking_sewio.gif)

_[img2]_

---

# BlueIOT

![bg right](assets/workers_tracking_bluiot.png)

_[img3]_

---

# Importance of Concept

#### Social

This concept would socially benefit with improving the safety of the workers causing less risk chances of injuries. This would motivate workers to work in a safer atmosphere knowing their lives are much better being watched over, would also raise their morale being aware that their employer is doing all this effort to ensure that they are as safe as possible.

#### Economical

When the workers are feeling safer they would work more efficiently

#### Environmental

Less accidents = more efficiency = less stress on environment

---
#### Video Tracking Option


![](assets/tracking.gif)

_[img4]_

---

#Sources

_[img1]_: https://www.tuv.com/world/en/construction-site-safety-training.html

_[img2]_: https://www.sewio.net/people-employee-indoor-location-tracking-and-monitoring/

_[img3]_: https://www.blueiot.com

_[img4]_: https://github.com/apoorvavinod/Real_time_Object_detection_and_tracking
